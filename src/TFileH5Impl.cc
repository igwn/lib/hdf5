
#include <RH5/TFileH5Impl.h>

TFileH5::Impl::Impl(const char *_fname, Option_t *option, Bool_t _sequential, int _verbosity)
{
    TString opt = option;
            opt.ToLower();

    fname = _fname;
    sequential = _sequential;
    verbosity = _verbosity;

    flags = 0;
    if(opt == "new" || opt == "create") flags = H5F_ACC_EXCL;
    if(opt == "recreate") flags = H5F_ACC_TRUNC;
    if(opt == "read" || opt.EqualTo("")) flags = H5F_ACC_RDONLY;
    if(opt == "update") flags = H5F_ACC_RDWR;

    this->h5 = new H5::H5File(fname, flags);
}

TString TFileH5::Impl::GetName()
{
    return this->fname;
}

void TFileH5::Impl::Close(Option_t *opt)
{
    if(h5) {
        h5->close();
        h5 = NULL;
    }
}

TFileH5::Impl::~Impl() { Close(); }

void TFileH5::Impl::ReversePtr(hsize_t* dim, int rank)
{
  hsize_t *begin_ptr, *end_ptr, ch;
 
  begin_ptr = dim;
  end_ptr = dim;
 
  // Move the end_ptr to the last character
  for (int i = 0; i < rank - 1; i++)
    end_ptr++;
 
  // Swap the char from start and end
  // index using begin_ptr and end_ptr
  for (int i = 0; i < rank / 2; i++) {
 
    // swap character
    ch = *end_ptr;
    *end_ptr = *begin_ptr;
    *begin_ptr = ch;
 
    // update pointers positions
    begin_ptr++;
    end_ptr--;
  }
}

char TFileH5::Impl::GetType(const H5::AbstractDs &d) {

  // Hint: H5 type
  const H5::DataType h5type = d.getDataType();
  if (h5type == H5::PredType::NATIVE_SHORT ) return 'B';
  if (h5type == H5::PredType::NATIVE_USHORT) return 'b';
  if (h5type == H5::PredType::NATIVE_INT   ) return h5type.getSize() == 16 ? 'S' : 'I';
  if (h5type == H5::PredType::NATIVE_UINT  ) return h5type.getSize() == 16 ? 's' : 'I';
  if (h5type == H5::PredType::NATIVE_LONG  ) return 'I';
  if (h5type == H5::PredType::NATIVE_ULONG ) return 'i';
  if (h5type == H5::PredType::NATIVE_FLOAT ) return 'F';
  if (h5type == H5::PredType::NATIVE_DOUBLE) return 'D';
  if (h5type == H5::PredType::NATIVE_CHAR  ) return 'B';
  if (h5type == H5::PredType::NATIVE_SCHAR ) return 'C';
  if (h5type == H5::PredType::NATIVE_HBOOL ) return 'O';

  // Hint: H5 class
  const H5T_class_t h5class = h5type.getClass();
  if(h5class == H5T_STRING ) return 'C';
  if(h5class == H5T_INTEGER) return 'I';
  if(h5class == H5T_FLOAT  ) return 'I';
  // if(h5class == H5T_BITFIELD ) return ""; // Bit field types
  // if(h5class == H5T_OPAQUE   ) return ""; // Opaque types
  // if(h5class == H5T_COMPOUND ) return ""; // Compound types
  // if(h5class == H5T_REFERENCE) return ""; // Reference types
  // if(h5class == H5T_ENUM		  ) return ""; // Enumeration types
  // if(h5class == H5T_VLEN	    ) return ""; // Variable-Length types
  // if(h5class == H5T_ARRAY	  ) return ""; // Array types

  return 0;
}

TObject *TFileH5::Impl::CreateScalar(TString objName, const H5::DataSet &dataSet)
{
    const H5::DataSpace dataSpace = dataSet.getSpace();
    Int_t rank = dataSpace.getSimpleExtentNdims();
    if(rank) return nullptr;

    char type = TFileH5::Impl::GetType(dataSet);
    switch(type) {

      case 'C':
      {
        std::string buf;
        dataSet.read(buf, dataSet.getDataType(), dataSpace);
        return new TNamed(objName, buf);
      }

      case 'I':
      {
        void *buf = malloc(sizeof(int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p = new TParameter<int>(objName, *((int*) buf));
        free(buf);

        return p;
      }

      case 'F':
      {
        void *buf = malloc(sizeof(float));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p = new TParameter<float>(objName, *((float*) buf));
        free(buf);

        return p;
      }

      case 'D':
      {
        void *buf = malloc(sizeof(double));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<double>(objName, *((double*) buf));
        free(buf);

        return p;
      }

      case 'L':
      {
        void *buf = malloc(sizeof(long long int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<long long int>(objName, *((long long int*) buf));
        free(buf);

        return p;
      }

      case 'G':
      {
        void *buf = malloc(sizeof(long int));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<long int>(objName, *((long int*) buf));
        free(buf);

        return p;
      }

      case 'O':
      {
        void *buf = malloc(sizeof(bool));
        dataSet.read(buf, dataSet.getDataType(), dataSpace);

        TObject *p =new TParameter<bool>(objName, *((bool*) buf));
        free(buf);

        return p;
      }

      default:
        return nullptr;
    }
}

TObject *TFileH5::Impl::CreateAttribute(TString objName, const H5::Attribute *attr)
{
    const H5::DataSpace dataSpace = attr->getSpace();

    char type = TFileH5::Impl::GetType(*attr);
    switch(type) {

      case 'C':
      {
        std::string buf;
        attr->read(attr->getDataType(), buf);
        return new TNamed(objName, buf);
      }

      case 'I':
      {
        void *buf = malloc(sizeof(int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<int>(objName, *((int*) buf));
        free(buf);

        return p;
      }

      case 'F':
      {
        void *buf = malloc(sizeof(float));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<float>(objName, *((float*) buf));
        free(buf);

        return p;
      }

      case 'D':
      {
        void *buf = malloc(sizeof(double));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<double>(objName, *((double*) buf));
        free(buf);

        return p;
      }

      case 'L':
      {
        void *buf = malloc(sizeof(long long int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<long long int>(objName, *((long long int*) buf));
        free(buf);

        return p;
      }

      case 'G':
      {
        void *buf = malloc(sizeof(long int));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<long int>(objName, *((long int*) buf));
        free(buf);

        return p;
      }

      case 'O':
      {
        void *buf = malloc(sizeof(bool));
        attr->read(attr->getDataType(), buf);

        TObject *p =new TParameter<bool>(objName, *((bool*) buf));
        free(buf);

        return p;
      }

      default:
        return nullptr;
    }
}

TObject *TFileH5::Impl::CreateObject(TString objName, const H5::DataSet &dataSet)
{
    //
    // Get object type
    char objType = TFileH5::Impl::GetType(dataSet);
    if  (objType == 0) {

      std::cout << " --> Skipping: Unsupported type (" << dataSet.getDataType().getClass() << ")! " << std::endl;
      return nullptr;
    }

    if (verbosity) std::cout << " TTree(type=\"" << objType << "\"";

    //
    // Rank gives the dimensionaly of the object
    H5::DataSpace dataSpace = dataSet.getSpace();
    Int_t rank = dataSpace.getSimpleExtentNdims();

    if (verbosity) std::cout << ", rank=" << rank;

    if(rank < 1) { // Scalar object

        if (verbosity) std::cout << ")" << std::endl;
        return TFileH5::Impl::CreateScalar(objName, dataSet);
    }

    // Find evidences for non sequential dataset
    if(sequential && rank < 2) {

      sequential = kFALSE;
      if(verbosity) std::cout << ", seq=" << sequential << "(forced)";

    } else if(verbosity) std::cout << ", seq=" << sequential;

    //
    // Dimension will store the number of elements in each dims
    hsize_t *dimSize   = new hsize_t[rank];
    dataSpace.getSimpleExtentDims(dimSize);

    //
    // Arrays to read out the data later on
    hsize_t *dimCount  = new hsize_t[rank];
    hsize_t *dimOffset = new hsize_t[rank];

    Int_t entries = 1;
    for (Int_t i = 0; i < rank; i++) {

        dimOffset[i] = 0;

        if (sequential && (i == 0)) dimCount[i] = 1;
        else dimCount[i] = dimSize[i];

        entries *= dimCount[i];
    }

    if (verbosity) {

        std::cout << ", entries=" << entries;
        if(rank > 1) {

            std::cout << "=[";
            for (Int_t i = 0; i < rank; i++) std::cout << dimSize[i] << (i < rank - 1 ? "x" : "");
            std::cout << "]";
        }
    }

    //
    // Prepare the description of the TTree branch of this dataset.
    size_t dataSize = dataSet.getDataType().getSize();
    if (verbosity) std::cout << ", basket=\"" << dataSize << " byte(s)\"";

    //
    // Define the branches
    TTree  *obj = new TTree(objName, objName);
            obj->SetAutoSave(0);

    TString objDesc = "";

    std::vector<int> dims = std::vector<int>(rank);
    for (Int_t i = (sequential ? 1 : 0); i < rank; i++) {

        TString branchName = "dim"+TString::Itoa(i,10);
        obj->Branch(branchName, &dims[i], branchName+"/I");
        objDesc  += branchName+"/I:";
    }

    void *data = malloc(dataSize);
    obj->Branch("data", data, "data/" + TString(objType));
    objDesc +=  "data/" + TString(objType);

    if (verbosity)
        std::cout << ", branch=\"" << objDesc.Data() << "\")" << std::endl;

    //
    // Index to dim vector converter
    TFileH5::Impl::ReversePtr(dimSize, rank); // Date space with multiple dimensions are stored in reversed order
    std::function<int(int N, hsize_t *D)> X = [&](int i, hsize_t *D) { return (i == 0) ? 1 : D[i-1] * X(i-1, D); };

    std::vector<int> Xn(1,1);
    for(int n = 0; n < rank-1; n++)
        Xn.push_back( X(n+1, dimSize) );

    // for(int n = 0; n < rank; n++)
    //     std::cout << Xn[n] << " >> ";

    //
    // Actually read and fill data
    H5::DataSpace *dataMem    = new H5::DataSpace(rank, dimCount);

    void *dataBuffer = malloc(dataSize*entries);
    if(sequential) {

        std::cerr << "Sequential processing not tested.." << std::endl;

        // /!\ SEQUENTIAL HDF5 IS UNTESTED
        // I didn't have sequential files..
        // So please.. send me an email (marco.meyer@cern.ch) with a sample if this attempt breaks..
        for (int iChunk = 0; iChunk < dimSize[0]; iChunk++) {

            H5::DataSpace dataSpace = dataSet.getSpace();
                          dataSpace.selectHyperslab(H5S_SELECT_SET, dimCount, dimOffset);

            const H5::DataType type = dataSet.getDataType();
            dataSet.read(dataBuffer, type, *(dataMem), dataSpace);

            int N0 = dimOffset[0];
            int Ni = dimOffset[0]+dimCount[0];
            for(int N = N0; N < Ni; N++) {

                TPrint::SetSingleCarriageReturn();
                TPrint::ProgressBar("Processing.. ", "", N+1, entries, 4096);

                for(int k = 0; k < rank; k++) // Index to dim vector conversion
                    dims[(rank-1)-k] = (N / Xn[k]) % dimSize[k];

                // for(int k = 0; k < rank; k++) // Index to dim vector conversion
                //     std::cout << dims[k] << "/" << rank << "  -- ";

                //std::cout << *((float*) data) << std::endl;

                memcpy(data, (void*) ((char*)dataBuffer + dataSize*N), dataSize); // Copy data behind data buffer pointer into tree linked variable
                obj->Fill();
            }

            dimOffset[0] = Ni;
        }

    } else {

        H5::DataSpace dataSpace = dataSet.getSpace();
                      dataSpace.selectHyperslab(H5S_SELECT_SET, dimCount, dimOffset);

        const H5::DataType type = dataSet.getDataType();
        dataSet.read(dataBuffer, type, *(dataMem), dataSpace);
        
        for(int N = 0; N < entries; N++) {

            TPrint::SetSingleCarriageReturn();
            TPrint::ProgressBar("Processing.. " + objName, "", N+1, entries, 4096);

            for(int k = 0; k < rank; k++) // Index to dim vector conversion
                dims[(rank-1)-k] = (N / Xn[k]) % dimSize[k];

            // for(int k = 0; k < rank; k++) // Index to dim vector conversion
            //     std::cout << dims[k] << "/" << rank << "  -- ";

            memcpy(data, (void*) ((char*)dataBuffer + dataSize*N), dataSize); // Copy data inside buffer linked with tree variables
            
            //std::cout << *((float*) data) << std::endl;
            obj->Fill();
        }
    }

    free(data);
    free(dataBuffer);

    delete dataMem;
    delete[] dimSize;
    delete[] dimCount;
    delete[] dimOffset;

    //
    // Objects and vectors for H5::Attributes readout in the current group
    TList *userInfo = obj->GetUserInfo();
    for (int i = 0; i < dataSet.getNumAttrs(); i++) {

        H5::Attribute *attr = new H5::Attribute(dataSet.openAttribute(i));

        if (verbosity > 1) std::cout << TPrint::kOrange << "\t* H5::Attribute #" << i+1 << ".. " << TPrint::kNoColor << attr->getName();
        H5::DataSpace attrSpace = attr->getSpace();

        char attrType = TFileH5::Impl::GetType(*attr);
        if(attrType == 0) {

          if (verbosity > 1) std::cout << " --> Skipping: Non-supported type! " << std::endl;
          return nullptr;
        }

        if (verbosity > 1) std::cout << "(type=\"" << attrType << "\"";

        //
        // Rank gives the dimensionaly of the object
        Int_t rank = attrSpace.getSimpleExtentNdims();
        if (verbosity > 1) std::cout << ", rank=" << rank << ")";

        TObject *objAttr = TFileH5::Impl::CreateAttribute(objName+"_"+attr->getName(), attr);
        if (verbosity > 1) std::cout << " : " << (objAttr == NULL ? "NULL" : objAttr->ClassName()) << std::endl;

        if(objAttr) {
          userInfo->Add(objAttr);
        }
    }

    return obj;
}

void TFileH5::Impl::Payload(H5::Group *group, bool bWrite)
{
    // Read attributes
    for (int idx = 0; idx < group->getNumAttrs(); idx++) {

      H5::Attribute attr = group->openAttribute(idx);

      TString objName = attr.getName();
      TObject *objAttr = TFileH5::Impl::CreateAttribute(objName, &attr);

      if(objAttr) {

        if (verbosity > 0) {

          std::cout << TPrint::kOrange << (!bWrite ? TString(fname) + ":/" : "") << (!bWrite ? TString(gDirectory->GetPath()).ReplaceAll("root:/", "") : TString(gDirectory->GetPath())) << "/" << objName  << TPrint::kNoColor << " " << objAttr->ClassName();

          if(objAttr->InheritsFrom(TParameter<int>::Class())) std::cout << "(value = \"" << ((TParameter<int> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TParameter<float>::Class())) std::cout << "(value = \"" << ((TParameter<float> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TParameter<double>::Class())) std::cout << "(value = \"" << ((TParameter<double> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TParameter<bool>::Class())) std::cout << "(value = \"" << ((TParameter<bool> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TParameter<long long int>::Class())) std::cout << "(value = \"" << ((TParameter<long long int> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TParameter<long int>::Class())) std::cout << "(\"" << ((TParameter<long int> *) objAttr)->GetVal() << "\")" << std::endl;
          else if(objAttr->InheritsFrom(TNamed::Class())) std::cout << "(value = \"" << ((TNamed *) objAttr)->GetTitle() << "\")" << std::endl;
          else std::cout << std::endl;
        }

        if(bWrite) gDirectory->WriteTObject(objAttr);
      }
    }
  
    // Read objects
    for (int idx = 0; idx < group->getNumObjs(); idx++) {

      TString pwd = gDirectory->GetPath();
      TString objName = group->getObjnameByIdx(idx);

      int objType = group->getObjTypeByIdx(idx);

      switch (objType)
      {
          case H5G_GROUP:
          {
              gDirectory->mkdir(objName);
              gDirectory->cd(objName);

              if(verbosity) std::cout << TPrint::kRed << (!bWrite ? TString(fname) + ":/" : "") << (!bWrite ? TString(gDirectory->GetPath()).ReplaceAll("root:/", "") : TString(gDirectory->GetPath())) << TPrint::kNoColor << std::endl;
              this->Payload(new H5::Group(group->openGroup(objName)), bWrite);
            
              gDirectory->cd(pwd);
          }
          break;

          case H5G_DATASET:
          {
              if(verbosity) std::cout << TPrint::kGreen << (!bWrite ? TString(fname) + ":/" : "") << (!bWrite ? TString(gDirectory->GetPath()).ReplaceAll("root:/", "") : TString(gDirectory->GetPath())) << "/" << objName << TPrint::kNoColor;

              TObject *obj = CreateObject(objName, group->openDataSet(objName));
              if(obj && bWrite) gDirectory->WriteTObject(obj);
          }
          break;

          default:
            std::cerr << TString("Unexpected H5 object type:").Append(TString::Itoa(objType,10)) << std::endl;
            throw std::exception();
        }
    }
}
