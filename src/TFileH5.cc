/**
 **********************************************
 *
 * \file TFileH5.cc
 * \brief Source code of the TFileH5 class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include <RH5/TFileH5Impl.h>
ClassImp(TFileH5)

TFileH5::TFileH5(const char *_fname, Option_t *option, Bool_t _sequential, int _verbosity) 
{
    this->pImpl = new TFileH5::Impl(_fname, option, _sequential, _verbosity);
};

TFileH5::~TFileH5()
{
    Close();
    if(this->pImpl) {
      
            delete this->pImpl;
            this->pImpl = NULL;
    }
}

void TFileH5::Print(Option_t *option)
{
    if(this->pImpl->h5 == NULL) {
      std::cerr << "H5 file is closed." << std::endl;
      return;
    } 
    
    UNUSED(option);
    std::cout << TPrint::kPurple << "Reading " << TPrint::kNoColor << this->GetName() << TPrint::kPurple << std::endl;
    this->pImpl->Payload(new H5::Group(this->pImpl->h5->openGroup("/")));
}

TString TFileH5::GetName()
{
  return this->pImpl->GetName();
}

void TFileH5::Save(Option_t *option, TString outfile) {

    if(this->pImpl->h5 == NULL) {
      std::cerr << "H5 file is closed." << std::endl;
      return;
    } 

    if (outfile.EqualTo(""))
        outfile = TString(gSystem->BaseName(this->GetName())).ReplaceAll(".h5", ".root").ReplaceAll(".hdf5", ".root").ReplaceAll(".hdf", ".root");

    std::cout << TPrint::kPurple << "Saving " << TPrint::kNoColor << this->GetName() << TPrint::kPurple << " into " << TPrint::kNoColor << outfile.Data() << " " << std::endl;

    TFile *root = new TFile(outfile, option, (TString) "HDF5: " + gSystem->BaseName(this->GetName()));
    this->pImpl->Payload(new H5::Group(this->pImpl->h5->openGroup("/")), true);
    root->Close();
}

void TFileH5::Close(Option_t *option) 
{ 
    this->pImpl->Close(option);
}

TObject *TFileH5::Read(TString path)
{
    UNUSED(path);
    std::cout << "TFileH5: Just do it !" << std::endl;

    return NULL;
}

void TFileH5::GetListOfKeys()
{
    std::cout << "TFileH5: Just do it !" << std::endl;
}
