/**
 **********************************************
 *
 * \file libRH5.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libRH5.Config.h"

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ namespace ROOT::RDF::RHdf5DS+;

#pragma link C++ namespace RH5+;
#pragma link C++ namespace TFileH5+;

#endif