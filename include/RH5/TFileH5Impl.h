#ifndef TFileH5Impl_H
#define TFileH5Impl_H

#include <Riostream.h>
#include <TObject.h>
#include <TString.h>
#include <TRandom.h>
#include <TFormula.h>

#include <TParameter.h>

#include <TPrint.h>

#include "H5Cpp.h"

#include <TFile.h>
#include <TTree.h>
#include <TObject.h>
#include <TSystem.h>

#include <exception>
#include <TFileH5.h>

#include <TPrint.h>

// /!\ SEQUENTIAL HDF5 IS UNTESTED
// I didn't have sequential files..
// So please.. send me an email (marco.meyer@cern.ch) with a sample if this breaks..

class TFileH5::Impl
{
    friend class TFileH5;
    protected:

        H5::H5File *h5 = NULL;

        const char *fname;
        unsigned int flags;
        int verbosity;
        Bool_t sequential;

    public:

        Impl(const char *_fname, Option_t *option, Bool_t _sequential, int _verbosity);
        ~Impl();

        void ReversePtr(hsize_t* dim, int rank);
        void Payload(H5::Group *group, bool bWrite = false);

        char GetType(const H5::AbstractDs &d);
        TObject *CreateScalar(TString objName, const H5::DataSet &dataSet);
        TObject *CreateAttribute(TString objName, const H5::Attribute *attr);
        TObject *CreateObject(TString objName, const H5::DataSet &dataSet);

        TString GetName();
        void Close(Option_t *option="");
};

#endif